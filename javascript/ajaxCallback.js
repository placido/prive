// src/ajaxbloc.js
import { spip as spip5 } from "config.js";

// src/ajaxform.js
import { spip as spip3 } from "config.js";

// src/anim.js
import { spip } from "config.js";
function positionner2(target, force2 = false, setfocus = true) {
  if (!target) {
    return;
  }
  let block = "nearest";
  if (force2) {
    block = "start";
    const p = target.getBoundingClientRect();
    if (target?.name !== "ancre_ajax" && p.height > 50 && p.height * 2 < window.innerHeight) {
      block = "center";
    }
  }
  const s = "--scroll";
  if (!target.classList.contains(s)) {
    target.classList.add(s);
    setTimeout(() => {
      target.classList.remove(s);
    }, 1000);
  }
  target.scrollIntoView({
    behavior: "smooth",
    block
  });
  if (setfocus) {
    const focusable = target.querySelector("input.text,textarea") ?? target;
    focusable.focus({ preventScroll: true });
  }
}
function animateLoading(target) {
  if (!target) {
    return;
  }
  target.setAttribute("aria-busy", "true");
  target.classList.add("loading");
  const ajax_image_searching = spip.ajax_image_searching || "⏳";
  let i = target.querySelector(".image_loading");
  if (i) {
    i.innerHTML = ajax_image_searching;
  } else {
    i = document.createElement("span");
    i.setAttribute("class", "image_loading");
    i.innerHTML = ajax_image_searching;
    const where = target.querySelector('.boutons [type="submit"].--clicked')?.parentNode ?? target;
    where.prepend(i);
  }
  return target;
}
function endLoading(target, hard = false) {
  if (!target) {
    return;
  }
  target.setAttribute("aria-busy", "false");
  target.classList.remove("loading");
  if (hard) {
    const i = target.querySelector(".image_loading");
    if (i) {
      i.parentNode.removeChild(i);
    }
  }
  return target;
}
function animateRemove(target, callback2) {
  if (!target) {
    return;
  }
  return new Promise((resolve, reject) => {
    target.classList.add(...["remove", "--animateRemove"]);
    setTimeout(() => {
      switch (typeof callback2) {
        case "function":
          callback2.apply(target);
          break;
        case false:
          target.style.display = "none";
          target.classList.remove("--animateRemove");
          break;
        default:
          target.parentNode.removeChild(target);
      }
      resolve(true);
    }, 950);
  });
}
function animateAppend(target, callback2) {
  if (!target) {
    return;
  }
  return new Promise((resolve, reject) => {
    target.classList.add(...["append", "--animateAppend"]);
    if (callback2 !== false) {
      positionner2(target, false);
    }
    setTimeout(() => {
      switch (typeof callback2) {
        case "function":
          callback2.apply(target);
          break;
        case false:
          break;
        default:
          target.classList.remove("--animateAppend");
          setTimeout(() => {
            target.classList.remove("append");
          }, 5000);
      }
      resolve(true);
    }, 1500);
  });
}

// src/log.js
import { spip as spip2 } from "config.js";

// src/url.js
function parametre_url2(url2, c2, v, sep, force_vide) {
  if (typeof url2 == "undefined") {
    url2 = "";
  }
  var p;
  var ancre = "";
  var a2 = "./";
  var args = [];
  p = url2.indexOf("#");
  if (p != -1) {
    ancre = url2.substring(p);
    url2 = url2.substring(0, p);
  }
  p = url2.indexOf("?");
  if (p !== -1) {
    if (p > 0)
      a2 = url2.substring(0, p);
    args = url2.substring(p + 1).split("&");
  } else
    a2 = url2;
  var regexp = new RegExp("^(" + c2.replace("[]", "\\[\\]") + "\\[?\\]?)(=.*)?$");
  var ajouts = [];
  var u = typeof v !== "object" ? encodeURIComponent(v) : v;
  var na = [];
  var v_read = null;
  for (var n = 0;n < args.length; n++) {
    var val = args[n];
    try {
      val = decodeURIComponent(val);
    } catch (e) {
    }
    var r = val.match(regexp);
    if (r && r.length) {
      if (v == null) {
        if (r[1].substr(-2) == "[]") {
          if (!v_read)
            v_read = [];
          v_read.push(r.length > 2 && typeof r[2] !== "undefined" ? r[2].substring(1) : "");
        } else {
          return r.length > 2 && typeof r[2] !== "undefined" ? r[2].substring(1) : "";
        }
      } else if (!v.length) {
      } else if (r[1].substr(-2) != "[]") {
        na.push(r[1] + "=" + u);
        ajouts.push(r[1]);
      }
    } else
      na.push(args[n]);
  }
  if (v == null)
    return v_read;
  if (v || v.length || typeof v === "number" || force_vide) {
    ajouts = "=" + ajouts.join("=") + "=";
    var all = c2.split("|");
    for (n = 0;n < all.length; n++) {
      if (ajouts.search("=" + all[n] + "=") == -1) {
        if (typeof v !== "object") {
          na.push(all[n] + "=" + u);
        } else {
          var id = all[n].substring(-2) == "[]" ? all[n] : all[n] + "[]";
          for (p = 0;p < v.length; p++)
            na.push(id + "=" + encodeURIComponent(v[p]));
        }
      }
    }
  }
  if (na.length) {
    if (!sep)
      sep = "&";
    a2 = a2 + "?" + na.join(sep);
  }
  return a2 + ancre;
}

// src/log.js
spip2.debug = spip2.debug || !!(parametre_url2(window.location.href, "var_mode") === "debug_js");
function log(...args) {
  if (spip2.debug && window?.console?.log) {
    window.console.log.apply(null, args);
  }
}

// src/css.js
function addCSS(payload, identifiant = "", force2 = false) {
  const id = identifiant ? `dyn_css_${identifiant}` : "dyn_css";
  const old = document.getElementById(id);
  if (payload && document.head && (!old || force2)) {
    if (force2) {
      old.parentNode.removeChild(old);
    }
    const style = document.createElement("style");
    style.setAttribute("id", id);
    style.innerHTML = payload;
    document.head.appendChild(style);
  }
}

// src/reader.js
var id = "virtualbufferupdate";
function initReaderBuffer() {
  if (document.getElementById(id)) {
    return;
  }
  addCSS(`#${id} {position:absolute;left:-9999em;}`, "virtualbuffer");
  const input = document.createElement("input");
  input.setAttribute("id", id);
  input.setAttribute("type", "hidden");
  input.setAttribute("value", 0);
  document.body.appendChild(input);
}
function updateReaderBuffer() {
  const buffer = document.getElementById(id);
  if (buffer) {
    buffer.value++;
  }
}

// src/ajaxform.js
function formulaire_switch_ajax(formElem, activer = true) {
  if (formElem.tagName !== "FORM") {
    log(`[formulaire_switch_ajax] ${formElem} n'est pas un form`);
    return;
  }
  const dejaAjax = formElem.querySelector('input[name="var_ajax"]');
  if (!dejaAjax && activer) {
    const hidden = document.createElement("input");
    hidden.setAttribute("name", "var_ajax");
    hidden.setAttribute("type", "hidden");
    hidden.setAttribute("value", "form");
    formElem.insertBefore(hidden, formElem.firstChild);
    formElem.addEventListener("submit", formulaire_on_submit);
  } else {
    dejaAjax?.parentNode.removeChild(dejaAjax);
    formElem.removeEventListener("submit", formulaire_on_submit);
  }
}
function formulaire_set_container(target) {
  if (!target.closest(".ajax-form-container")) {
    for (const balise_js of [...target.querySelectorAll("script")]) {
      balise_js.parentNode.removeChild(balise_js);
    }
    const container = document.createElement("div");
    container.classList.add("ajax-form-container");
    let aria = target.dataset.aria;
    if (aria && typeof aria === "object") {
      for (const i in aria) {
        container.setAttribute(i, aria[i]);
      }
    } else {
      aria = false;
    }
    target.parentNode.insertBefore(container, target);
    container.appendChild(target);
    if (aria) {
      target.querySelector("form:not([aria-live])").setAttribute("aria-live", "off");
    }
  }
}
function formulaire_dyn_ajax(formParent) {
  initReaderBuffer();
  formulaire_set_container(formParent);
  for (const formElem of [
    ...formParent.querySelectorAll("form:not(.noajax):not(.bouton_action_post)")
  ]) {
    formulaire_switch_ajax(formElem, true);
    formElem.classList.add(...["noajax", "hasajax"]);
  }
}
function formulaire_fix_markup_post_submit(formParent) {
  const parent = formParent;
  if (!parent)
    return;
  const br = parent.querySelector("br.bugajaxie");
  if (br) {
    br.parentNode.removeChild(br);
  }
  const re = new RegExp(/\s--\w*/g);
  let child = parent?.firstElementChild;
  if (child?.className === "ajax") {
    parent.innerHTML = child.firstElementChild.innerHTML;
    child = parent?.firstElementChild;
  }
  if (parent.id && parent.id === child?.id || parent.classList.contains("ajax") && child.classList.contains("ajax") || parent.className && parent.className.replace(re, "") === child.className.replace(re, "")) {
    parent.innerHTML = child.innerHTML;
  }
}
async function formulaire_on_submit(event) {
  event.preventDefault();
  const formElem = event.target;
  const formParent = formElem.closest(".formulaire_spip");
  const formContainer = formParent.closest(".ajax-form-container");
  const submitter = event.submitter;
  const scrollwhensubmit = !formElem.classList.contains("noscroll") && !submitter?.classList.contains("noscroll");
  const params = new URLSearchParams([...new FormData(formElem).entries()]);
  if (submitter.type === "submit" && submitter.name) {
    params.append(submitter.name, submitter.value);
  }
  submitter.classList.add("--clicked");
  log(["formulaire_on_submit", formElem, event, params]);
  animateLoading(formContainer);
  fetch(formElem.action, { method: "POST", body: params }).then((response) => {
    if (!response.ok) {
      formulaire_on_error(formElem, `${response?.statusText.length ? response?.statusText : "Err."} ${response.status}`);
      throw new Error(`[formulaire_dyn_ajax] ${formElem.action} ${response.status} ${response.statusText}`);
    }
    return response.text();
  }).then((data2) => {
    const signature = data2.slice(0, 100);
    if (signature.match(/^\s*noajax\s*$/)) {
      formulaire_switch_ajax(formElem, false);
      formParent.classList.add("resubmit-noajax");
      if (submitter.type === "submit" && submitter.name) {
        submitter.setAttribute("type", "hidden");
      }
      setTimeout(() => {
        try {
          formElem.submit();
        } catch (err) {
          const submitFn = document.createElement("form").submit();
          submitFn.apply(formElem);
        }
      }, 50);
    } else {
      if (!data2.length || !signature.includes("ajax-form-is-ok")) {
        if (scrollwhensubmit) {
          positionner(formParent, true);
        }
        formulaire_on_error(formElem, spip3.alerts.error_on_ajaxform);
        return;
      }
      const preloaded = spip3.preloaded_urls;
      spip3.preloaded_urls = {};
      setInnerHTML(formParent, data2);
      formulaire_fix_markup_post_submit(formParent);
      const a2 = formParent.querySelector("a");
      if (a2?.matches("[name=ajax_redirect]")) {
        const redirect = a2.getAttribute("href");
        setTimeout(() => {
          const cur = window.location.href.split("#");
          document.location.replace(redirect);
          if (cur[0] === redirect.split("#")[0]) {
            window.location.reload();
          }
        }, 10);
        animateLoading(formContainer);
      } else {
        endLoading(formContainer, true);
        if (scrollwhensubmit) {
          positionner(formParent, true);
        }
      }
      if (!formParent.querySelector(".reponse_formulaire_ok")) {
        spip3.preloaded_urls = preloaded;
      }
      updateReaderBuffer();
      triggerAjaxLoad(formParent);
    }
  });
}
async function formulaire_bouton_action_post(btn_action, blocfrag2, ajax_env) {
  if (!btn_action || !ajax_env || !blocfrag2) {
    return;
  }
  const url2 = btn_action.getAttribute("action").split("#");
  const submitter = btn_action.querySelector(".submit");
  const scrollwhensubmit = !btn_action.classList.contains("noscroll") && !submitter?.classList.contains("noscroll");
  const template = document.createElement("template");
  template.innerHTML = `
\t\t<input type="hidden" name="var_ajax" value="1" />
\t\t<input type="hidden" name="var_ajax_env" value="${ajax_env}"/>
\t\t` + (url2[1] ? `<input type="hidden" name="var_ajax_ancre" value="${url2[1]}" />` : ``);
  btn_action.insertBefore(template.content, btn_action.firstChild);
  btn_action.addEventListener("submit", async (e) => {
    e.preventDefault();
    submitter.classList.add("--clicked");
    animateLoading(btn_action);
    const confirm2 = await new Promise((resolve, reject) => {
      resolve(formulaire_bouton_action_confirm(submitter));
    });
    if (!confirm2) {
      endLoading(btn_action, true);
      return;
    }
    const action = btn_action.getAttribute("action");
    const params = new URLSearchParams([...new FormData(e.target).entries()]);
    log(["bouton_action_post", btn_action, action, params, confirm2]);
    fetch(action, { method: "POST", body: params }).then((response) => {
      spip3.preloaded_urls = {};
      if (!response.ok) {
        endLoading(btn_action, true);
        const redirect = parametre_url(url2, "redirect") ?? window.location.href;
        on_ajax_failed(blocfrag2, response.status, redirect, {
          history: response.status === 400
        });
        throw new Error(`[bouton_action_post] ${action} ${response.status} ${response.statusText}`);
      }
      animateLoading(blocfrag2);
      return response.text();
    }).then((data2) => {
      if (data2.length) {
        on_ajax_loaded(blocfrag2, data2, undefined, {
          noscroll: !scrollwhensubmit
        });
      }
    });
  });
  btn_action.classList.add("bind-ajax");
}
async function formulaire_bouton_action_confirm(bouton_action) {
  if (bouton_action?.dataset.callback) {
    const data = JSON.parse(bouton_action.dataset.callback);
    const confirm = data?.confirm ? window.confirm(data.confirm) : true;
    const exec = data?.exec ? eval(data.exec) : true;
    return confirm && exec;
  } else {
    return true;
  }
}
function formulaire_on_error(formElem, message_erreur = "Erreur") {
  const formContainer = formElem?.closest(".ajax-form-container");
  if (formContainer) {
    endLoading(formContainer, true);
  }
  for (const alert of [
    ...formElem.parentNode.querySelectorAll('.error,[class^="reponse_"]')
  ]) {
    alert.parentNode.removeChild(alert);
  }
  const template = document.createElement("template");
  template.innerHTML = `
\t<div class="msg-alert error" role="alert" data-alert="error">
\t\t<div class="msg-alert__text clearfix">${message_erreur}</div>
\t</div>
\t`;
  formElem.insertBefore(template.content.children[0], formElem.firstChild);
  formulaire_switch_ajax(formElem, false);
}

// src/history.js
import { spip as spip4 } from "config.js";
function setHistoryState(blocfrag2) {
  if (!window.history.replaceState)
    return;
  if (!blocfrag2.getAttribute("id")) {
    spip4.stateId = [...document.querySelectorAll('[id^="ghsid"]')].length;
    blocfrag2.setAttribute("id", `ghsid${++spip4.stateId}`);
  }
  const href2 = blocfrag2.dataset.url || blocfrag2.dataset.origin;
  const state = {
    id: blocfrag2.getAttribute("id"),
    href: href2
  };
  const ajaxid = blocfrag2.getAttribute("class").match(/\bajax-id-[\w-]+\b/);
  if (ajaxid?.length) {
    state.ajaxid = ajaxid[0];
  }
  window.history.replaceState(state, window.document.title, window.document.location);
}
function pushHistoryState(href2, title) {
  if (!window.history.pushState) {
    return false;
  }
  window.history.pushState({}, title, href2);
}
window.onpopstate = (popState) => {
  if (popState.state?.href) {
    log(["popState", popState]);
    let blocfrag2 = false;
    for (const bloc_id of popState.state.id.split(" ")) {
      blocfrag2 = document.getElementById(bloc_id);
      ajaxClick(blocfrag2, popState.state.href, { history: false });
      if (!blocfrag2) {
        window.location.href = popState.state.href;
        break;
      }
    }
  }
};

// src/ajaxbloc.js
function ajaxReload(_blocfrag, options2 = {}) {
  let blocfrag2 = _blocfrag;
  if (blocfrag2 && typeof blocfrag2 === "string") {
    blocfrag2 = document.querySelector(`div.ajaxbloc.ajax-id-${blocfrag2}`) ?? document.querySelector(blocfrag2);
  }
  if (blocfrag2 && typeof blocfrag2 === "object" && !blocfrag2.classList.contains("ajaxbloc")) {
    blocfrag2 = blocfrag2.closest(".ajaxbloc");
  }
  if (!blocfrag2) {
    log(`[ajaxReload]: blocfrag ${blocfrag2} non trouvé`);
    return;
  }
  const ajax_env = blocfrag2?.dataset?.ajaxEnv;
  if (!ajax_env)
    return;
  let href2 = options2.href || blocfrag2.dataset.url || blocfrag2.dataset.origin;
  if (href2 && typeof href2 !== "undefined") {
    if (href2.indexOf("var_nullify") !== -1) {
      href2 = parametre_url2(href2, "var_nullify", "");
    }
    const callback2 = options2.callback || null;
    const history2 = options2.history || false;
    const args = options2.args || {};
    for (const key in args) {
      const force_vide = Array.isArray(args[key]) && !args[key].length;
      const v = [null, undefined].includes(args[key]) || force_vide ? "" : args[key];
      href2 = parametre_url2(href2, key, v, "&", force_vide);
    }
    const url2 = makeAjaxUrl(href2, ajax_env, blocfrag2.dataset.origin);
    loadAjax(blocfrag2, url2, href2, {
      force: true,
      callback: callback2,
      history: history2
    });
    return true;
  }
}
function ajaxbloc(blocfrag2, options2 = {}) {
  for (const bloc_child of [
    ...blocfrag2.querySelectorAll(".ajaxbloc:not(.bind-ajax)")
  ]) {
    ajaxbloc(bloc_child);
  }
  const ajax_env = blocfrag2?.dataset?.ajaxEnv;
  if (!ajax_env)
    return;
  if (!blocfrag2.classList.contains("bind-ajaxReload")) {
    blocfrag2.addEventListener("ajaxReload", (event, options3) => {
      if (ajaxReload(blocfrag2, options3)) {
        event.stopPropagation();
      }
    });
    blocfrag2.classList.add("bind-ajaxReload");
  }
  for (const selecteur of [
    ...blocfrag2.querySelectorAll(spip5.ajaxbloc_selecteur)
  ].filter((e) => !e.classList.contains("noajax")).filter((e) => !e.classList.contains("bind-ajax"))) {
    selecteur.addEventListener("click", (event) => {
      event.stopPropagation();
      event.preventDefault();
      const force2 = selecteur.classList.contains("nocache");
      const history2 = !selecteur.classList.contains("nohistory") || !!blocfrag2.closest(".box_modalbox");
      const noscroll2 = selecteur.classList.contains("noscroll");
      const opt = Object.assign({}, { force: force2, history: history2, noscroll: noscroll2 }, options2);
      return ajaxClick(blocfrag2, selecteur.href, opt);
    });
    if (selecteur.classList.contains("preload")) {
      const url2 = makeAjaxUrl(selecteur.href, ajax_env, blocfrag2.dataset.origin);
      if (!spip5.preloaded_urls[url2]) {
        spip5.preloaded_urls[url2] = "<!--loading-->";
        fetch(url2).then((response) => {
          if (!response.ok) {
            spip5.preloaded_urls[url2] = "";
            on_ajax_failed(blocfrag2, response.status, href, options2);
            throw new Error(`[preloaded_url] $url ${response.status} ${response.statusText}`);
          }
          return response.text();
        }).then((data2) => {
          spip5.preloaded_urls[url2] = data2;
        });
      }
    }
    selecteur.classList.add("bind-ajax");
  }
  for (const btn_action of [
    ...blocfrag2.querySelectorAll("form.bouton_action_post.ajax:not(.noajax):not(.bind-ajax)")
  ]) {
    formulaire_bouton_action_post(btn_action, blocfrag2, ajax_env);
  }
}
function makeAjaxUrl(href2, ajax_env, origin) {
  const url2 = href2.split("#") || [];
  url2[0] = parametre_url2(url2[0], "var_ajax", 1);
  url2[0] = parametre_url2(url2[0], "var_ajax_env", ajax_env);
  if (origin) {
    if (origin.indexOf("var_nullify") !== -1) {
      origin = parametre_url2(origin, "var_nullify", "");
    }
    let p = origin.indexOf("?");
    const nullify = [];
    if (p !== -1) {
      const args = origin.substring(p + 1).split("&");
      let val;
      let arg;
      for (let n = 0;n < args.length; n++) {
        arg = args[n].split("=");
        arg = decodeURIComponent(arg[0]);
        p = arg.indexOf("[");
        if (p !== -1) {
          arg = arg.substring(0, p);
        }
        val = parametre_url2(href2, arg);
        if ((typeof val === "undefined" || val == null) && nullify.indexOf(arg) === -1) {
          nullify.push(arg);
        }
      }
      if (nullify.length) {
        url2[0] = `${url2[0]}&var_nullify=${encodeURIComponent(nullify.join("|"))}`;
      }
    }
  }
  if (url2[1]) {
    url2[0] = parametre_url2(url2[0], "var_ajax_ancre", url2[1]);
  }
  return url2[0];
}
function ajaxClick(blocfrag2, href2, options2 = {}) {
  const ajax_env = blocfrag2?.dataset?.ajaxEnv;
  if (!ajax_env)
    return;
  const url2 = makeAjaxUrl(href2, ajax_env, blocfrag2.dataset.origin);
  loadAjax(blocfrag2, url2, href2, options2);
  return false;
}
function loadAjax(blocfrag, url, href, options = {}) {
  const callback = eval(blocfrag.dataset.loadingCallback) || null;
  const force = options.force || false;
  if (callback) {
    callback.apply(this, [blocfrag, url, href, options]);
  } else {
    animateLoading(blocfrag);
  }
  if (spip5.preloaded_urls[url] && !force) {
    if (spip5.preloaded_urls[url] === "<!--loading-->") {
      setTimeout(() => {
        loadAjax(blocfrag, url, href, options);
      }, 100);
      return;
    }
    on_ajax_loaded(blocfrag, spip5.preloaded_urls[url], href, options);
  } else {
    const d = new Date;
    spip5.preloaded_urls[url] = "<!--loading-->";
    fetch(parametre_url2(url, "var_t", d.getTime())).then((response) => {
      if (!response.ok) {
        spip5.preloaded_urls[url] = "";
        on_ajax_failed(blocfrag, response.status, href, options);
        throw new Error(`[loadAjax] ${response.status} ${response.statusText}`);
      }
      return response.text();
    }).then((data2) => {
      on_ajax_loaded(blocfrag, data2, href, options);
      spip5.preloaded_urls[url] = data2;
      if (options.callback && typeof options.callback === "function") {
        options.callback.apply(this, [blocfrag, data2, href, options]);
      }
    });
  }
}
function followLink(target) {
  target.click();
  if (!target.classList.contains("bind-ajax")) {
    window.location.href = target.getAttribute("href");
  }
}
function onAjaxLoad(f) {
  spip5.load_handlers.push(f);
}
function triggerAjaxLoad(root) {
  log(["triggerAjaxLoad", root, spip5.load_handlers]);
  for (let i = 0;i < spip5.load_handlers.length; i++) {
    spip5.load_handlers[i].apply(this, [root]);
  }
}
function setInnerHTML(blocfrag2, data2) {
  const parser = new DOMParser;
  const input = parser.parseFromString(data2, "text/html");
  const balises = [];
  for (const old of [...input.getElementsByTagName("script")]) {
    const exec2 = document.createElement("script");
    for (const attr of [...old.attributes]) {
      exec2.setAttribute(attr.name, attr.value);
    }
    const scriptText = document.createTextNode(old.textContent);
    old.remove();
    exec2.appendChild(scriptText);
    balises.push(exec2);
  }
  blocfrag2.innerHTML = input.documentElement.innerHTML;
  for (const balise of balises) {
    blocfrag2.append(balise);
  }
}
function on_ajax_loaded(blocfrag, c, href, options = { history: false }) {
  log(["on_ajax_loaded", blocfrag, c, href, options]);
  const history = !href ? false : options.history;
  const noscroll = options.noscroll || false;
  if (history) {
    setHistoryState(blocfrag);
  }
  let callback = blocfrag.dataset.loadedCallback || false;
  if (callback) {
    callback = eval(callback);
    callback.call(blocfrag, c, href, history);
  } else {
    setInnerHTML(blocfrag, c);
    endLoading(blocfrag);
  }
  if (typeof href !== "undefined") {
    blocfrag.dataset.url = href;
  }
  if (history) {
    let href_history = href;
    if (href_history.indexOf("var_nullify") !== -1) {
      href_history = parametre_url2(href_history, "var_nullify", "");
    }
    pushHistoryState(href_history);
    setHistoryState(blocfrag);
  }
  if (!noscroll) {
    positionner2(blocfrag);
  }
  triggerAjaxLoad(blocfrag);
  const a = blocfrag.closest("form.hasajax");
  if (a) {
    a.classList.remove("noajax");
    const dyn_form = a.closest("div.ajax");
    if (dyn_form) {
      formulaire_dyn_ajax(dyn_form);
    }
  }
  updateReaderBuffer();
}
function on_ajax_failed(blocfrag2, statusCode, href2, options2 = { history: false }) {
  log(`Echec AJAX statusCode ${statusCode}`);
  blocfrag2.classList.add("invalid");
  endLoading(blocfrag2);
  if (options2.history) {
    window.location.href = href2;
  }
}
// src/cvt_verifier.js
function formulaire_verifier(form, callback2, champ) {
  if (form.closest(".ajax-form-container").dataset.ariaBusy !== "true") {
    if (form.classList.contains("hasajax")) {
      const params = new URLSearchParams([...new FormData(form).entries()]);
      params.append("formulaire_action_verifier_json", true);
      fetch(form.action, {
        method: "POST",
        body: params
      }).then((response) => {
        if (!response.ok) {
          throw new Error(`[formulaire_verifier] ${form.action} ${response.status} ${response.statusText}`);
        }
        return response.json();
      }).then((data2) => {
        if (form.closest(".ajax-form-container").dataset.ariaBusy !== "true") {
          callback2.apply(null, [form, data2, champ]);
        }
      });
    } else {
      callback2.apply(null, [form, { message_erreur: "form non ajax" }, champ]);
    }
  }
}
function formulaire_activer_verif_auto(target, callback2) {
  callback2 = callback2 || formulaire_actualiser_erreurs;
  formulaire_set_container(target);
  const form = target.matches("form") ? target : target.querySelector("form");
  const activer = () => {
    if (form.getAttribute("data-verifjson") !== "on") {
      form.dataset.verifjson = "on";
      form.addEventListener("change", (e) => {
        if (e.target.matches('input:not([type="hidden"]),select,textarea')) {
          setTimeout(() => {
            formulaire_verifier(form, callback2, e.target.getAttribute("name"));
          }, 50);
        }
      });
    }
  };
  activer();
  onAjaxLoad(() => {
    setTimeout(activer, 150);
  });
}
function formulaire_actualiser_erreurs(target, erreurs) {
  log(["formulaire_actualiser_erreurs", target, erreurs]);
  const formParent = target.classList.contains("formulaire_spip") ? target : target.closest(".formulaire_spip");
  if (!formParent)
    return;
  for (const reponse of [
    ...formParent.querySelectorAll(".reponse_formulaire,.erreur_message")
  ]) {
    reponse.parentNode.removeChild(reponse);
  }
  for (const err of [...formParent.querySelectorAll(".erreur")]) {
    err.classList.remove("erreur");
  }
  if (erreurs.message_ok) {
    const _ok = document.createElement("div");
    _ok.setAttribute("class", "reponse_formulaire reponse_formulaire_ok");
    _ok.innerHTML = erreurs.message_ok;
    formParent.insertBefore(_ok, formParent.firstChild);
  }
  if (erreurs.message_erreur) {
    const _err = document.createElement("div");
    _err.setAttribute("class", "reponse_formulaire reponse_formulaire_erreur");
    _err.innerHTML = erreurs.message_erreur;
    formParent.insertBefore(_err, formParent.firstChild);
  }
  for (const k in erreurs) {
    const saisie = formParent.querySelector(`.editer_${k}`);
    if (saisie) {
      saisie.classList.add("erreur");
      const _err_saisie = document.createElement("span");
      _err_saisie.setAttribute("class", "erreur_message");
      _err_saisie.innerHTML = erreurs[k];
      saisie.querySelector("label").parentNode.insertBefore(_err_saisie, saisie.querySelector("label").nextElementSibling);
    }
  }
}
// src/perf.js
function throttle(callback2, delay) {
  var last;
  var timer;
  return function() {
    var now = +new Date;
    var args = arguments;
    if (last && now < last + delay) {
      clearTimeout(timer);
      timer = setTimeout(() => {
        last = now;
        callback2.apply(this, args);
      }, delay);
    } else {
      last = now;
      callback2.apply(this, args);
    }
  };
}
function debounce(callback2, delay) {
  var timer;
  return function() {
    var args = arguments;
    clearTimeout(timer);
    timer = setTimeout(() => {
      callback2.apply(this, args);
    }, delay);
  };
}
// src/dom-slide.js
async function slideUp(element, duration = 500) {
  return new Promise((resolve, reject) => {
    element.style.height = `${element.offsetHeight}px`;
    element.style.transitionProperty = "height, margin, padding";
    element.style.transitionDuration = `${duration}ms`;
    element.offsetHeight;
    element.style.overflow = "hidden";
    element.style.height = 0;
    element.style.paddingTop = 0;
    element.style.paddingBottom = 0;
    element.style.marginTop = 0;
    element.style.marginBottom = 0;
    window.setTimeout(() => {
      element.style.display = "none";
      element.style.removeProperty("height");
      element.style.removeProperty("padding-top");
      element.style.removeProperty("padding-bottom");
      element.style.removeProperty("margin-top");
      element.style.removeProperty("margin-bottom");
      element.style.removeProperty("overflow");
      element.style.removeProperty("transition-duration");
      element.style.removeProperty("transition-property");
      resolve(false);
    }, duration);
  });
}
HTMLElement.prototype.slideUp = async function(duration) {
  await slideUp(this, duration);
};
async function slideDown(element, duration = 500) {
  return new Promise((resolve, reject) => {
    element.style.removeProperty("display");
    let display = window.getComputedStyle(element).display;
    if (display === "none")
      display = "block";
    element.style.display = display;
    const height = element.offsetHeight;
    element.style.overflow = "hidden";
    element.style.height = 0;
    element.style.paddingTop = 0;
    element.style.paddingBottom = 0;
    element.style.marginTop = 0;
    element.style.marginBottom = 0;
    element.offsetHeight;
    element.style.transitionProperty = "height, margin, padding";
    element.style.transitionDuration = `${duration}ms`;
    element.style.height = `${height}px`;
    element.style.removeProperty("padding-top");
    element.style.removeProperty("padding-bottom");
    element.style.removeProperty("margin-top");
    element.style.removeProperty("margin-bottom");
    window.setTimeout(() => {
      element.style.removeProperty("height");
      element.style.removeProperty("overflow");
      element.style.removeProperty("transition-duration");
      element.style.removeProperty("transition-property");
      resolve(true);
    }, duration);
  });
}
HTMLElement.prototype.slideDown = async function(duration) {
  await slideDown(this, duration);
};
async function slideToggle(element, duration = 500) {
  if (window.getComputedStyle(element).display === "none") {
    return await slideDown(element, duration);
  } else {
    return await slideUp(element, duration);
  }
}
HTMLElement.prototype.slideToggle = async function(duration) {
  await slideToggle(this, duration);
};
export {
  triggerAjaxLoad,
  throttle,
  slideUp,
  slideToggle,
  slideDown,
  positionner2 as positionner,
  parametre_url2 as parametre_url,
  onAjaxLoad,
  log,
  formulaire_verifier,
  formulaire_set_container,
  formulaire_dyn_ajax,
  formulaire_actualiser_erreurs,
  formulaire_activer_verif_auto,
  followLink,
  endLoading,
  debounce,
  animateRemove,
  animateLoading,
  animateAppend,
  ajaxbloc,
  ajaxReload,
  ajaxClick,
  addCSS
};
