// src/puces.js
import { spip } from "config.js";
import { parametre_url } from "ajaxCallback.js";

// src/hoverClass.js
import { throttle } from "ajaxCallback.js";
function hoverClass(target, _toggleClass) {
  const toggleClass = _toggleClass || "on";
  target.addEventListener("mouseover", throttle(() => {
    target.classList.add(toggleClass);
  }, 100));
  target.addEventListener("mouseout", throttle(() => {
    target.classList.remove(toggleClass);
  }, 100));
}
HTMLElement.prototype.hoverClass = function(toggleClass) {
  return hoverClass(this, toggleClass);
};
NodeList.prototype.hoverClass = function(toggleClass) {
  this.forEach((el) => {
    el.hoverClass(toggleClass);
  });
};

// src/puces.js
function selec_statut(id, type, decal, puce, script) {
  const node = document.querySelector(`.imgstatut${type}${id}`);
  const accepter_change_statut = confirm(spip.alerts.confirm_changer_statut);
  if (!accepter_change_statut || !node) {
    return;
  }
  const statutdecal = document.querySelector(`.statutdecal${type}${id}`);
  const w = window.getComputedStyle(statutdecal).getPropertyValue("width") || "auto";
  statutdecal.style = `margin-left:${decal};width:${w};`;
  statutdecal.classList.remove("on");
  fetch(script).then((response) => {
    return response.text();
  }).then((data) => {
    if (!data) {
      node.setAttribute("src", puce);
    } else {
      const r = window.open();
      r.document.write(data);
      r.document.close();
    }
  });
}
function prepare_selec_statut(node, nom, type, id, action) {
  hoverClass(node);
  action = parametre_url(action, "type", type);
  action = parametre_url(action, "id", id);
  fetch(action, { method: "GET" }).then((response) => {
    return response.text();
  }).then((data) => {
    node.innerHTML = data;
    puce_survol(node);
  });
}
function puce_survol(_root) {
  const root = _root || document;
  for (const popup of [
    ...root.querySelectorAll("span.puce_objet_popup a")
  ].filter((e) => !e.classList.contains("puce-survol-enabled"))) {
    popup.addEventListener("click", (e) => {
      e.preventDefault();
      e.stopPropagation();
      selec_statut(popup.getAttribute("data-puce-id"), popup.getAttribute("data-puce-type"), popup.getAttribute("data-puce-decal"), popup.firstChild.src, popup.getAttribute("data-puce-action"));
    });
    popup.classList.add("puce-survol-enabled");
  }
  for (const hover of [...root.querySelectorAll("span.puce_objet")].filter((e) => !e.classList.contains("puce-survol-enabled"))) {
    hover.addEventListener("mouseover", (e) => {
      e.stopPropagation();
      if (hover.getAttribute("data-puce-loaded") === null) {
        prepare_selec_statut(hover, hover.getAttribute("data-puce-nom"), hover.getAttribute("data-puce-type"), hover.getAttribute("data-puce-id"), hover.getAttribute("data-puce-action"));
        hover.setAttribute("data-puce-loaded", "");
      }
    });
    hover.classList.add("puce-survol-enabled");
  }
}
// src/logo.js
import { throttle as throttle2 } from "ajaxCallback.js";
function logo_survol(target) {
  target.addEventListener("mouseenter", throttle2(() => {
    if (target.dataset.srcHover) {
      target.setAttribute("data-src-original", "src");
      target.setAttribute("src", "data-src-hover");
    }
  }, 100));
  target.addEventListener("mouseleave", throttle2(() => {
    if (target.dataset.srcOriginal) {
      target.setAttribute("src", "data-src-original");
    }
  }, 100));
}
// src/depliants_legacy.js
import { slideDown, slideUp, throttle as throttle3 } from "ajaxCallback.js";
function depliant(origine, _cible) {
  if (!origine.classList.contains("depliant")) {
    const time = 400;
    let t = null;
    const cible = _cible || document.querySelector(origine.dataset.depliant);
    origine.classList.add("depliant");
    origine.addEventListener("mouseenter", throttle3(() => {
      if (origine.classList.contains("replie")) {
        t = setTimeout(() => {
          t = null;
          showother(origine, cible);
        }, time);
      }
    }, 200));
    origine.addEventListener("mouseleave", throttle3(() => {
      if (t) {
        clearTimeout(t);
        t = null;
      }
    }, 200));
  }
}
async function showother(origine, cible) {
  if (origine.classList.contains("togglewait")) {
    return false;
  }
  if (origine.classList.contains("replie")) {
    origine.classList.remove("replie");
    origine.classList.add(...["deplie", "togglewait"]);
    await slideDown(cible);
    cible.classList.add("blocdeplie");
    cible.classList.remove("blocreplie");
    origine.classList.remove("togglewait");
  }
}
async function hideother(origine, cible) {
  if (origine.classList.contains("togglewait")) {
    return false;
  }
  if (!origine.classList.contains("replie")) {
    origine.classList.remove("deplie");
    origine.classList.add(...["replie", "togglewait"]);
    await slideUp(cible);
    cible.classList.add("blocreplie");
    cible.classList.remove("blocdeplie");
    origine.classList.remove("togglewait");
  }
}
function toggleother(origine, cible) {
  return origine.classList.contains("replie") ? showother(origine, cible) : hideother(origine, cible);
}
function activer_depliants(_root) {
  const root = _root || document;
  for (const depliant_survol of [
    ...root.querySelectorAll("[data-depliant]")
  ].filter((e) => !e.classList.contains("depliant"))) {
    depliant(depliant_survol);
    if (depliant_survol.dataset.fixDepliIncertain !== "undefined") {
      const cible = document.querySelector(depliant_survol.dataset.depliant);
      if (cible?.offsetParent) {
        depliant_survol.classList.add("deplie");
        depliant_survol.classList.remove("replie");
      }
    }
  }
  for (const btn_clicancre of [
    ...root.querySelectorAll("[data-depliant-clicancre]")
  ].filter((e) => !e.classList.contains("depliant"))) {
    ["click", "keydown"].forEach((evt) => {
      btn_clicancre.addEventListener(evt, throttle3((e) => {
        e.preventDefault();
        btn_clicancre.classList.add("depliant");
        const origine = e.target.parentNode.dataset.depliant !== "undefined" ? e.target.parentNode : btn_clicancre;
        const cible = document.querySelector(btn_clicancre.dataset.depliantClicancre) || document.querySelector(origine.dataset.depliant);
        toggleother(origine, cible);
      }, 250));
    });
  }
}
// src/reloadExecPage.js
import { ajaxReload } from "ajaxCallback.js";
function reloadExecPage(exec, _blocs) {
  const blocs = _blocs || "#navigation,#extra";
  for (const bloc of document.querySelectorAll(blocs)) {
    const ajaxbloc = bloc.firstElementChild;
    if (ajaxbloc) {
      ajaxReload(bloc.firstElementChild, { args: { exec } });
    }
  }
  document.body.classList.toggle("edition", exec.match(/_edit$/));
}
export {
  reloadExecPage,
  puce_survol,
  logo_survol,
  hoverClass,
  activer_depliants
};
