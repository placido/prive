import {formulaire_dyn_ajax} from 'ajaxCallback.js';
function cibler_ajaxform(_root) {
	const root = _root || document;
	for (const form of [
		...root.querySelectorAll("form:not(.bouton_action_post)")
	]) {
		const dynform = form.closest('div.ajax');
		if (dynform) {
			formulaire_dyn_ajax(dynform);
		}
	}
}
addCSS(spip.css.ajax,'ajax');
cibler_ajaxform();
onAjaxLoad(cibler_ajaxform);