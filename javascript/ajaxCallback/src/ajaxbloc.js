import { spip } from "config.js";
import {
	formulaire_dyn_ajax,
	formulaire_bouton_action_post,
	formulaire_bouton_action_confirm,
} from "./ajaxform.js";
import { animateLoading, endLoading, positionner } from "./anim.js";
import { pushHistoryState, setHistoryState } from "./history.js";
import { log } from "./log.js";
import { updateReaderBuffer } from "./reader.js";
import { parametre_url } from "./url.js";

/**
 * fonction appelee sur l'evenement ajaxReload d'un bloc ajax
 * que l'on declenche quand on veut forcer sa mise a jour
 *
 * @param object|string blocfrag
 * @param object options
 *   callback : fonction appelee apres le rechargement
 *   href : url to load instead of origin url
 *   args : arguments passes a l'url rechargee (permet une modif du contexte)
 *   history : bool to specify if navigation history is modified by reload or not (false if not provided)
 */
export function ajaxReload(_blocfrag, options = {}) {
	let blocfrag = _blocfrag;
	// blocfrag peut aussi désigner ajaxid de INCLURE{ajax=ajaxid}
	if (blocfrag && typeof blocfrag === "string") {
		blocfrag =
			document.querySelector(`div.ajaxbloc.ajax-id-${blocfrag}`) ??
			document.querySelector(blocfrag);
	}
	// cas rare où le bloc désigné existe bel et bien, mais n'est pas rechargeable
	// on se rabat sur le premier parent rechargeable
	if (
		blocfrag &&
		typeof blocfrag === "object" &&
		!blocfrag.classList.contains("ajaxbloc")
	) {
		blocfrag = blocfrag.closest(".ajaxbloc");
	}
	if (!blocfrag) {
		log(`[ajaxReload]: blocfrag ${blocfrag} non trouvé`);
		return;
	}
	const ajax_env = blocfrag?.dataset?.ajaxEnv;
	if (!ajax_env) return;
	let href = options.href || blocfrag.dataset.url || blocfrag.dataset.origin;
	if (href && typeof href !== "undefined") {
		if (href.indexOf("var_nullify") !== -1) {
			href = parametre_url(href, "var_nullify", "");
		}
		const callback = options.callback || null;
		const history = options.history || false;
		const args = options.args || {};
		for (const key in args) {
			const force_vide = Array.isArray(args[key]) && !args[key].length;
			const v =
				[null, undefined].includes(args[key]) || force_vide ? "" : args[key];
			href = parametre_url(href, key, v, "&", force_vide);
		}
		const url = makeAjaxUrl(href, ajax_env, blocfrag.dataset.origin);
		// recharger sans historisation dans l'url
		loadAjax(blocfrag, url, href, {
			force: true,
			callback: callback,
			history: history,
		});
		return true;
	}
}

/**
 * Appliquer un comportement ajax
 * sur les <INCLURE{fond=...,env,ajax}>
 *
 * @param object blocfrag
 * @param object options
 * @return void
 */
export function ajaxbloc(blocfrag, options = {}) {
	// traiter les enfants d'abord :
	// un lien ajax provoque le rechargement
	// du plus petit bloc ajax le contenant
	for (const bloc_child of [
		...blocfrag.querySelectorAll(".ajaxbloc:not(.bind-ajax)"),
	]) {
		ajaxbloc(bloc_child);
	}
	const ajax_env = blocfrag?.dataset?.ajaxEnv;
	if (!ajax_env) return;

	// "No aria is better than bad aria"
	// blocfrag.setAttribute("aria-live", "polite");
	// blocfrag.setAttribute("aria-atomic", "true");
	// dans un formulaire, le screen reader relit tout a chaque saisie d'un caractere si on est en aria-live
	// mettre un aria-live="off" sur les forms inclus dans ce bloc aria-live="polite"
	//for (let non_aria_form of [...blocfrag.querySelectorAll('form:not([aria-live])')]) {
	//	non_aria_form..setAttribute("aria-live", "off");
	//}

	// blocfrag devient rechargeable manuellement
	if (!blocfrag.classList.contains("bind-ajaxReload")) {
		blocfrag.addEventListener("ajaxReload", (event, options) => {
			if (ajaxReload(blocfrag, options)) {
				// don't trig reload of parent blocks
				event.stopPropagation();
			}
		});
		blocfrag.classList.add("bind-ajaxReload");
	}

	// certains liens provoquent le rechargement au clic
	for (const selecteur of [
		...blocfrag.querySelectorAll(spip.ajaxbloc_selecteur),
	]
		.filter((e) => !e.classList.contains("noajax"))
		.filter((e) => !e.classList.contains("bind-ajax"))) {
		selecteur.addEventListener("click", (event) => {
			event.stopPropagation();
			event.preventDefault();
			const force = selecteur.classList.contains("nocache");
			const history =
				!selecteur.classList.contains("nohistory") ||
				!!blocfrag.closest(".box_modalbox");
			const noscroll = selecteur.classList.contains("noscroll");
			const opt = Object.assign(
				{},
				{ force: force, history: history, noscroll: noscroll },
				options,
			);
			return ajaxClick(blocfrag, selecteur.href, opt);
		});
		// et le contenu peut être préchargé
		if (selecteur.classList.contains("preload")) {
			const url = makeAjaxUrl(
				selecteur.href,
				ajax_env,
				blocfrag.dataset.origin,
			);
			if (!spip.preloaded_urls[url]) {
				spip.preloaded_urls[url] = "<!--loading-->";
				fetch(url)
					.then((response) => {
						if (!response.ok) {
							spip.preloaded_urls[url] = "";
							on_ajax_failed(blocfrag,response.status,href,options);
							throw new Error(
								`[preloaded_url] $url ${response.status} ${response.statusText}`,
							);
						}
						return response.text();
					})
					.then((data) => {
						spip.preloaded_urls[url] = data;
					});
			}
		}
		// previent qu'on ajax pas deux fois le meme lien
		selecteur.classList.add("bind-ajax");
	} // fin ajaxbloc_selecteur

	// preparer les boutons d'action
	for (const btn_action of [
		...blocfrag.querySelectorAll(
			"form.bouton_action_post.ajax:not(.noajax):not(.bind-ajax)",
		),
	]) {
		formulaire_bouton_action_post(btn_action, blocfrag, ajax_env);
	}
}

/**
 * Calculer l'url ajax a partir de l'url du lien
 * et de la variable d'environnement du bloc ajax
 * passe aussi l'ancre eventuelle sous forme d'une variable
 * pour que le serveur puisse la prendre en compte
 * et la propager jusqu'a la reponse
 * sous la forme d'un lien cache
 *
 * @param string href
 * @param string ajax_env
 * @param string origin
 * @return string
 */
export function makeAjaxUrl(href, ajax_env, origin) {
	const url = href.split("#") || [];
	url[0] = parametre_url(url[0], "var_ajax", 1);
	url[0] = parametre_url(url[0], "var_ajax_env", ajax_env);

	// les arguments de origin qui ne sont pas dans href doivent etre explicitement fournis vides dans url
	if (origin) {
		if (origin.indexOf("var_nullify") !== -1) {
			origin = parametre_url(origin, "var_nullify", "");
		}
		let p = origin.indexOf("?");
		const nullify = [];
		if (p !== -1) {
			// recuperer la base
			const args = origin.substring(p + 1).split("&");
			let val;
			let arg;
			for (let n = 0; n < args.length; n++) {
				arg = args[n].split("=");
				arg = decodeURIComponent(arg[0]);
				p = arg.indexOf("[");
				if (p !== -1) {
					arg = arg.substring(0, p);
				}
				val = parametre_url(href, arg);
				if (
					(typeof val === "undefined" || val == null) &&
					nullify.indexOf(arg) === -1
				) {
					nullify.push(arg);
				}
			}
			if (nullify.length) {
				url[0] = `${url[0]}&var_nullify=${encodeURIComponent(nullify.join("|"))}`;
			}
		}
	}
	if (url[1]) {
		url[0] = parametre_url(url[0], "var_ajax_ancre", url[1]);
	}
	return url[0];
}

/**
 * fonction appelee sur l'evenement click d'un lien ajax
 *
 * @param object blocfrag
 *   objet Element qui cible le bloc ajax contenant
 * @param string href
 *   url du lien a suivre
 * @param object options
 *   force : pour interdire l'utilisation du cache
 *   history : pour interdire la mise en historique
 */
export function ajaxClick(blocfrag, href, options = {}) {
	const ajax_env = blocfrag?.dataset?.ajaxEnv;
	if (!ajax_env) return;
	/* // deprecated
	if (!window.ajax_confirm) {
		// on rearme pour le prochain clic
		window.ajax_confirm=true;
		let d = new Date();
		// seule une annulation par confirm() dans les 2 secondes precedentes est prise en compte
		if ((d.getTime()-window.ajax_confirm_date)<=2)
			return false;
	}
	*/
	const url = makeAjaxUrl(href, ajax_env, blocfrag.dataset.origin);
	loadAjax(blocfrag, url, href, options);
	return false;
}

/**
 * Articuler le rechargement d'un bloc ajax
 * @param object blocfrag
 *   bloc cible
 * @param string url
 *   url pour la requete ajax
 * @param string href
 *   url du lien clique
 * @param object options
 *   bool force : pour forcer la requete sans utiliser le cache
 *   function callback : callback au retour du chargement
 *   bool history : prendre en charge l'historisation dans l'url
 */
export function loadAjax(blocfrag, url, href, options = {}) {
	const callback = eval(blocfrag.dataset.loadingCallback) || null;
	const force = options.force || false;
	if (callback) {
		callback.apply(this,[blocfrag, url, href, options]);
	} else {
		animateLoading(blocfrag);
	}
	if (spip.preloaded_urls[url] && !force) {
		// si on est deja en train de charger ce fragment, revenir plus tard
		if (spip.preloaded_urls[url] === "<!--loading-->") {
			setTimeout(() => {
				loadAjax(blocfrag, url, href, options);
			}, 100);
			return;
		}
		on_ajax_loaded(blocfrag, spip.preloaded_urls[url], href, options);
	} else {
		const d = new Date();
		spip.preloaded_urls[url] = "<!--loading-->";

		fetch(parametre_url(url, "var_t", d.getTime()))
			.then((response) => {
				if (!response.ok) {
					spip.preloaded_urls[url] = "";
					on_ajax_failed(blocfrag, response.status, href, options);
					throw new Error(
						`[loadAjax] ${response.status} ${response.statusText}`,
					);
				}
				return response.text();
			})
			.then((data) => {
				on_ajax_loaded(blocfrag, data, href, options);
				spip.preloaded_urls[url] = data;
				if (options.callback && typeof options.callback === "function") {
					options.callback.apply(this, [blocfrag, data, href, options]);
				}
			});
	}
}

/**
 * Suivre un lien en simulant le click sur le lien
 * Si le lien est ajax, on se contente de declencher l'evenement click()
 * Si le lien est non ajax, on finit en remplacant l'url de la page
 *
 * @param {HTMLElement} target
 */
export function followLink(target) {
	target.click();
	if (!target.classList.contains("bind-ajax")) {
		window.location.href = target.getAttribute("href");
	}
}

/**
 * Ajoute la fonction passée en paramètre à la pile
 * de celles qui seront exécutées à la suite d'un rechargement ajax
 *
 * @param function f
 */
export function onAjaxLoad(f) {
	spip.load_handlers.push(f);
}

/**
 * Exécute à tour de rôle les fonctions attachées via onAjaxLoad
 *
 * La plupart du temps, la fonction f s'applique au contenu nouvellement chargé,
 * au document racine dans son ensemble si non précisé.
 *
 * @param root
 */
export function triggerAjaxLoad(root) {
	log(["triggerAjaxLoad", root, spip.load_handlers]);
	for (let i = 0; i < spip.load_handlers.length; i++) {
		spip.load_handlers[i].apply(this,[root]);
	}
}

/**
 * Insérer une chaîne comme nouveau contenu d'un élément (innerHTML) et
 * faire en sorte d'évaluer le code des balises <script/> présentes
 *
 * @param object blocfrag
 * @param string data
 */
export function setInnerHTML(blocfrag, data) {
	const parser = new DOMParser();
	const input = parser.parseFromString(data, "text/html");
	const balises = [];

	for (const old of [...input.getElementsByTagName("script")]) {
		const exec = document.createElement("script");
		for (const attr of [...old.attributes]) {
			exec.setAttribute(attr.name, attr.value);
		}
		const scriptText = document.createTextNode(old.textContent);
		old.remove();
		exec.appendChild(scriptText);
		balises.push(exec);
	}
	blocfrag.innerHTML = input.documentElement.innerHTML;
	for (const balise of balises) {
		blocfrag.append(balise);
	}
}

/**
 * Afficher dans la page
 * le html d'un bloc ajax charge
 * @param object blocfrag
 * @param string c
 * @param string href
 * @param object options
 */
export function on_ajax_loaded(
	blocfrag,
	c,
	href,
	options = { history: false },
) {
	log(["on_ajax_loaded", blocfrag, c, href, options]);

	const history = !href ? false : options.history;
	const noscroll = options.noscroll || false;
	if (history) {
		setHistoryState(blocfrag);
	}
	let callback = blocfrag.dataset.loadedCallback || false;
	if (callback) {
		callback = eval(callback);
		callback.call(blocfrag, c, href, history);
	} else {
		setInnerHTML(blocfrag, c);
		endLoading(blocfrag);
	}

	if (typeof href !== "undefined") {
		blocfrag.dataset.url = href;
	}

	if (history) {
		let href_history = href;
		if (href_history.indexOf("var_nullify") !== -1) {
			href_history = parametre_url(href_history, "var_nullify", "");
		}
		pushHistoryState(href_history);
		setHistoryState(blocfrag);
	}
	if (!noscroll) {
		positionner(blocfrag);
	}

	triggerAjaxLoad(blocfrag);
	// si le fragment ajax est dans un form ajax,
	// il faut remettre a jour les evenements attaches
	// car le fragment peut comporter des submit ou button
	const a = blocfrag.closest("form.hasajax");
	if (a) {
		a.classList.remove("noajax");
		const dyn_form = a.closest("div.ajax");
		if (dyn_form) {
			formulaire_dyn_ajax(dyn_form);
		}
	}
	updateReaderBuffer();
}

/**
 * Traitement en fallback en cas d'erreur ajax
 * @param object blocfrag
 * @param integer statusCode
 * @param string href
 * @param object options
 */
export function on_ajax_failed(
	blocfrag,
	statusCode,
	href,
	options = { history: false },
) {
	log(`Echec AJAX statusCode ${statusCode}`);
	// marquer le bloc invalide
	blocfrag.classList.add("invalid");
	endLoading(blocfrag);
	// quelle que soit l'erreur, on redirige si c'était la nouvelle URL principale de la page
	if (options.history) {
		window.location.href = href;
	}
}
