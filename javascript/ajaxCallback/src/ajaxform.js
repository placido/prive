import { spip } from "config.js";
import { animateLoading, endLoading } from "./anim.js";
import {
	on_ajax_failed,
	on_ajax_loaded,
	setInnerHTML,
	triggerAjaxLoad,
} from "./ajaxbloc.js";
import { log } from "./log.js";
import { initReaderBuffer, updateReaderBuffer } from "./reader.js";

/**
 * Activer / désactiver le comportement ajaxé sur un formulaire
 *
 * @param  {HMTLElement} formElem <form>
 * @param  {Boolean} activer   [true = activer ajax]
 */
export function formulaire_switch_ajax(formElem, activer = true) {
	if (formElem.tagName !== "FORM") {
		log(`[formulaire_switch_ajax] ${formElem} n'est pas un form`);
		return;
	}
	const dejaAjax = formElem.querySelector('input[name="var_ajax"]');
	if (!dejaAjax && activer) {
		const hidden = document.createElement("input");
		hidden.setAttribute("name", "var_ajax");
		hidden.setAttribute("type", "hidden");
		hidden.setAttribute("value", "form");
		formElem.insertBefore(hidden, formElem.firstChild);
		formElem.addEventListener("submit", formulaire_on_submit);
	} else {
		dejaAjax?.parentNode.removeChild(dejaAjax);
		formElem.removeEventListener("submit", formulaire_on_submit);
	}
}

/**
 * Englobe un formulaire dans un conteneur `ajax-form-container`
 * afin de circonscrire la portée des modifications du DOM
 *  ```
 *	.ajax-form-container[aria-busy]
 *		> .ajax
 *	 		> .formulaire_spip
 *	  			> form
 *  ```
 *
 * @param  {HTMLElement} target référence à englober
 */
export function formulaire_set_container(target) {
	if (!target.closest(".ajax-form-container")) {
		// supprimer les balise scripts
		for (const balise_js of [...target.querySelectorAll("script")]) {
			balise_js.parentNode.removeChild(balise_js);
		}

		const container = document.createElement("div");
		container.classList.add("ajax-form-container");
		let aria = target.dataset.aria;
		if (aria && typeof aria === "object") {
			for (const i in aria) {
				container.setAttribute(i, aria[i]);
			}
		} else {
			aria = false;
		}
		target.parentNode.insertBefore(container, target);
		container.appendChild(target);
		if (aria) {
			// dans un formulaire, le screen reader relit tout a chaque saisie d'un caractere si on est en aria-live
			target
				.querySelector("form:not([aria-live])")
				.setAttribute("aria-live", "off");
		}
	}
}

/**
 * Cherche les balises <form> depuis formParent
 * et leur assigne un comportement dynamique
 *
 * @param  {HTMLElement} formParent
 */
export function formulaire_dyn_ajax(formParent) {
	initReaderBuffer();
	formulaire_set_container(formParent);

	for (const formElem of [
		...formParent.querySelectorAll(
			"form:not(.noajax):not(.bouton_action_post)",
		),
	]) {
		formulaire_switch_ajax(formElem, true);
		// previent qu'on n'ajaxera pas deux fois le meme formulaire en cas de ajaxload
		// mais le marquer comme ayant l'ajax au cas ou on reinjecte du contenu ajax dedans
		formElem.classList.add(...["noajax", "hasajax"]);
	}
}

/**
 * Normalise le markup d'un retour de formulaire en excluant les div.ajax redondantes
 *
 * @param  {HTMLElement} formParent <div.spip_formulaire>
 */
export function formulaire_fix_markup_post_submit(formParent) {
	const parent = formParent;
	if (!parent) return;

	// retrait du marqueur qui a deja rempli son role
	const br = parent.querySelector("br.bugajaxie");
	if (br) {
		br.parentNode.removeChild(br);
	}

	const re = new RegExp(/\s--\w*/g);
	let child = parent?.firstElementChild;
	// Si div.ajax est à l'intérieur du formulaire
	// il faut désemboiter sur 2 niveaux
	// <formParent> > div.ajax > .spip_formulaire > form ===> <formParent> > form
	if (child?.className === "ajax") {
		parent.innerHTML = child.firstElementChild.innerHTML;
		child = parent?.firstElementChild; // faire suivre la référence child
	}
	// Si parent/enfant ont les mêmes classes (hors classes `--truc` qui est un marqueur d'animation en cours)
	// ou si la class ajax est redondante
	// on désemboite sur 1 niveau
	// <formParent> > .spip_formulaire.ajax > spip_formulaire.ajax > form ===> <formParent> > .spip_formulaire.ajax > form
	if (
		(parent.id && parent.id === child?.id) ||
		(parent.classList.contains("ajax") && child.classList.contains("ajax")) ||
		(parent.className &&
			parent.className.replace(re, "") === child.className.replace(re, ""))
	) {
		parent.innerHTML = child.innerHTML;
	}
}

/**
 * Action standard de soumission d'un formulaire ajaxé (hors bouton_action)
 *
 * @param  {Object} event [événement déclencheur]
 */
export async function formulaire_on_submit(event) {
	event.preventDefault();
	const formElem = event.target;
	const formParent = formElem.closest(".formulaire_spip");
	const formContainer = formParent.closest(".ajax-form-container");
	const submitter = event.submitter;
	const scrollwhensubmit =
		!formElem.classList.contains("noscroll") &&
		!submitter?.classList.contains("noscroll");
	const params = new URLSearchParams([...new FormData(formElem).entries()]);
	// on rajoute la donnée name|value du bouton cliqué
	if (submitter.type === "submit" && submitter.name) {
		params.append(submitter.name, submitter.value);
	}
	submitter.classList.add("--clicked");

	log(["formulaire_on_submit", formElem, event, params]);

	animateLoading(formContainer);
	//fetch('https://httpbin.org/status/500', {method:"POST", body:params})
	fetch(formElem.action, { method: "POST", body: params })
		.then((response) => {
			if (!response.ok) {
				formulaire_on_error(
					formElem,
					`${response?.statusText.length ? response?.statusText : "Err."} ${response.status}`,
				);
				throw new Error(
					`[formulaire_dyn_ajax] ${formElem.action} ${response.status} ${response.statusText}`,
				);
			}
			return response.text();
		})
		.then((data) => {
			const signature = data.slice(0, 100);
			// const signature = 'noajax'; // pour simuler un formulaire à resservir sans ajax
			// le serveur ne veut pas traiter ce formulaire en ajax
			// on resubmit tout de suite sans ajax
			if (signature.match(/^\s*noajax\s*$/)) {
				formulaire_switch_ajax(formElem, false);
				formParent.classList.add("resubmit-noajax");
				// faire suivre la valeur du bouton à l'origine de
				// l'evenement submit en le convertissant en input hidden
				if (submitter.type === "submit" && submitter.name) {
					submitter.setAttribute("type", "hidden");
				}
				setTimeout(() => {
					try {
						formElem.submit();
					} catch (err) {
						// just in case form has element with name/id of 'submit'
						const submitFn = document.createElement("form").submit();
						submitFn.apply(formElem);
					}
				}, 50);
			} else {
				if (!data.length || !signature.includes("ajax-form-is-ok")) {
					if (scrollwhensubmit) {
						positionner(formParent, true);
					}
					formulaire_on_error(formElem, spip.alerts.error_on_ajaxform);
					return;
				}
				// on commence par vider le cache des urls, si jamais un js au retour
				// essaye tout de suite de suivre un lien en cache
				// dans le doute sur la validite du cache il vaut mieux l'invalider
				const preloaded = spip.preloaded_urls;
				spip.preloaded_urls = {};

				setInnerHTML(formParent, data);
				formulaire_fix_markup_post_submit(formParent);

				// chercher une ancre de redirection
				const a = formParent.querySelector("a");
				if (a?.matches("[name=ajax_redirect]")) {
					const redirect = a.getAttribute("href");
					setTimeout(() => {
						const cur = window.location.href.split("#");
						document.location.replace(redirect);
						// regarder si c'est juste un changement d'ancre : dans ce cas il faut reload
						// (le faire systematiquement provoque des bugs)
						if (cur[0] === redirect.split("#")[0]) {
							window.location.reload();
						}
					}, 10);
					// ne pas arreter l'etat loading, puisqu'on redirige !
					// mais le relancer car l'image loading a pu disparaitre
					animateLoading(formContainer);
				} else {
					endLoading(formContainer, true);
					if (scrollwhensubmit) {
						positionner(formParent, true);
					}
				}

				if (!formParent.querySelector(".reponse_formulaire_ok")) {
					spip.preloaded_urls = preloaded;
				}
				// mettre a jour le buffer du navigateur pour aider jaws et autres readers
				updateReaderBuffer();
				triggerAjaxLoad(formParent);
			}
		});
}

/**
 * Preparer les boutons d'action qui sont techniquement
 * des form minimaux mais se comportent comme des liens
 *
 * @param  {HTMLElement} btn_action <form>
 * @param  {HTMLElement} blocfrag <div.ajaxbloc/> le plus proche
 * @param  {string} ajax_env
 */
export async function formulaire_bouton_action_post(
	btn_action,
	blocfrag,
	ajax_env,
) {
	if (!btn_action || !ajax_env || !blocfrag) {
		return;
	}
	const url = btn_action.getAttribute("action").split("#");
	const submitter = btn_action.querySelector(".submit");
	const scrollwhensubmit =
		!btn_action.classList.contains("noscroll") &&
		!submitter?.classList.contains("noscroll");
	const template = document.createElement("template");
	template.innerHTML =
		`
		<input type="hidden" name="var_ajax" value="1" />
		<input type="hidden" name="var_ajax_env" value="${ajax_env}"/>
		` +
		(url[1]
			? `<input type="hidden" name="var_ajax_ancre" value="${url[1]}" />`
			: ``);
	btn_action.insertBefore(template.content, btn_action.firstChild);

	btn_action.addEventListener("submit", async (e) => {
		e.preventDefault();
		submitter.classList.add("--clicked");
		animateLoading(btn_action);
		const confirm = await new Promise((resolve, reject) => {
			resolve(formulaire_bouton_action_confirm(submitter));
		});
		if (!confirm) {
			endLoading(btn_action, true);
			return; // action interrompue, dans le <Dialog> ou la fonction préliminaire
		}
		const action = btn_action.getAttribute("action");
		const params = new URLSearchParams([...new FormData(e.target).entries()]);
		log(["bouton_action_post", btn_action, action, params, confirm]);
		//fetch('https://httpbin.org/status/500', {method:"POST", body:params})
		fetch(action, { method: "POST", body: params })
			.then((response) => {
				spip.preloaded_urls = {}; // on vide le cache des urls car on a fait une action en bdd
				if (!response.ok) {
					endLoading(btn_action, true);
					const redirect =
						parametre_url(url, "redirect") ?? window.location.href;
					// si c'est une erreur 400 c'est un fragment ajax invalide, il faut rediriger vers href
					on_ajax_failed(blocfrag, response.status, redirect, {
						history: response.status === 400,
					});
					throw new Error(
						`[bouton_action_post] ${action} ${response.status} ${response.statusText}`,
					);
				}
				animateLoading(blocfrag);
				return response.text();
			})
			.then((data) => {
				if (data.length) {
					on_ajax_loaded(blocfrag, data, undefined, {
						noscroll: !scrollwhensubmit,
					});
				}
			});
	});
	// on ajax pas deux fois le meme lien
	btn_action.classList.add("bind-ajax");
}

/**
 * Déclencher l'affichage du message de confirmation et l'éxecution d'une fonction,
 * préalablement à la soumission POST d'un bouton d'action
 *
 * @param {HTMLElement} bouton_action
 * @return boolean
 */
export async function formulaire_bouton_action_confirm(bouton_action) {
	if (bouton_action?.dataset.callback) {
		const data = JSON.parse(bouton_action.dataset.callback);
		const confirm = data?.confirm ? window.confirm(data.confirm) : true;
		const exec = data?.exec ? eval(data.exec) : true;
		//console.log([data,confirm,exec,confirm && exec])
		return confirm && exec;
	} else {
		return true;
	}
}

/**
 * Afficher un message d'erreur global sur un formulaire ajax
 * Masque les événtuels messages et alertes précédents
 * Rétablit le formulaire en mode noajax
 *
 * @param  {HTMLElement} formElem <form>
 * @param  {String} message_erreur
 */
export function formulaire_on_error(formElem, message_erreur = "Erreur") {
	const formContainer = formElem?.closest(".ajax-form-container");
	if (formContainer) {
		endLoading(formContainer, true);
	}
	for (const alert of [
		...formElem.parentNode.querySelectorAll('.error,[class^="reponse_"]'),
	]) {
		alert.parentNode.removeChild(alert);
	}
	const template = document.createElement("template");
	template.innerHTML = `
	<div class="msg-alert error" role="alert" data-alert="error">
		<div class="msg-alert__text clearfix">${message_erreur}</div>
	</div>
	`;
	formElem.insertBefore(template.content.children[0], formElem.firstChild);
	formulaire_switch_ajax(formElem, false); // resservir en mode noajax
}
