import { spip } from "config.js";

/**
 * Défilement de l'écran pour garder le contact visuel
 * sur l'élément ciblé
 *
 * @element.scrollIntoView
 * https://developer.mozilla.org/fr/docs/Web/API/Element/scrollIntoView
 *
 * @param object target
 * @param bool force
 * @param bool setfocus
 */
export function positionner(target, force = false, setfocus = true) {
	if (!target) {
		return;
	}
	// block = alignement vertical
	let block = "nearest"; // ramener dans le viewport au plus rapide

	if (force) {
		block = "start"; // aligner l'élément et le haut du viewport
		const p = target.getBoundingClientRect();
		// si la cible est pertinente, et sa hauteur reste petite
		// au regard de celle du viewport, on choisit de la centrer
		if (
			target?.name !== "ancre_ajax" &&
			p.height > 50 &&
			p.height * 2 < window.innerHeight
		) {
			block = "center";
		}
	}
	// une classe temporaire pour le style (scroll-margin)
	const s = '--scroll';
	if (!target.classList.contains(s)) {
		target.classList.add(s)
		setTimeout(() => {
			target.classList.remove(s);
		}, 1000);
	}

	target.scrollIntoView({
		behavior: "smooth",
		block: block,
	});

	if (setfocus) {
		const focusable = target.querySelector("input.text,textarea") ?? target;
		focusable.focus({ preventScroll: true });
	}
}

/**
 * Animation du bloc cible pour faire patienter
 * Cherche ou rajoute un loader .image_loading
 *
 * @param object target
 */
export function animateLoading(target) {
	if (!target) {
		return;
	}
	target.setAttribute("aria-busy", "true");
	target.classList.add("loading");
	const ajax_image_searching = spip.ajax_image_searching || "⏳";
	let i = target.querySelector(".image_loading");
	if (i) {
		i.innerHTML = ajax_image_searching;
	} else {
		i = document.createElement("span");
		i.setAttribute("class", "image_loading");
		i.innerHTML = ajax_image_searching;
		const where =
			target.querySelector('.boutons [type="submit"].--clicked')?.parentNode ?? target;
		where.prepend(i);
	}
	return target;
}

/**
 * Fin de l'animation
 * l'argument permet de forcer le raz du contenu s'il est inchange
 *
 * @param object target
 * @param hard
 */
export function endLoading(target, hard = false) {
	if (!target) {
		return;
	}
	target.setAttribute("aria-busy", "false");
	target.classList.remove("loading");
	if (hard) {
		const i = target.querySelector(".image_loading");
		if (i) {
			i.parentNode.removeChild(i);
		}
	}
	return target;
}

/**
 * Animation d'un item que l'on supprime
 *
 * @param object target
 * @param {false|function} callback : (false masque l'élément au lieu de le supprimer)
 * @returns {null|Promise<boolean>}
 */
export function animateRemove(target, callback) {
	if (!target) {
		return;
	}
	return new Promise((resolve, reject) => {
		target.classList.add(...["remove", "--animateRemove"]);
		setTimeout(() => {
			switch (typeof callback) {
				case "function":
					callback.apply(target);
					break;
				case false:
					target.style.display = "none";
					target.classList.remove("--animateRemove");
					break;
				default:
					target.parentNode.removeChild(target);
			}
			resolve(true);
		}, 950);
	});
}

/**
 *  Animation d'un item que l'on ajoute
 *
 * @param object target
 * @param {false|function} callback  (false désactive le repositionnement et la fin de l'animation)
 * @returns {null|Promise<boolean>}
 */
export function animateAppend(target, callback) {
	if (!target) {
		return;
	}
	return new Promise((resolve, reject) => {
		target.classList.add(...["append", "--animateAppend"]);
		if (callback !== false) {
			positionner(target, false);
		}
		setTimeout(() => {
			switch (typeof callback) {
				case "function":
					callback.apply(target);
					break;
				case false:
					break;
				default:
					target.classList.remove("--animateAppend");
					setTimeout(() => {
						target.classList.remove("append");
					}, 5000);
			}
			resolve(true);
		}, 1500);
	});
}

